var app = angular.module('myApp', [ 'multipleSelect' ]);
app
		.controller(
				'myCtrl',
				function($scope, $http) {

					$http.get('cities.json').then(function(res) {
						$scope.cities = res.data;
					});

					document.getElementById("select").onchange = function() {
						getCityWeatherInfo();
					};

					function getCityWeatherInfo() {
						var x = document.getElementById("select");
						$http(
								{
									method : "GET",
									url : "http://api.openweathermap.org/data/2.5/weather?id="
											+ x.value
											+ "&units=metric&appid=221c07652b86b24392f518958c8a65bd"
								}).then(function mySucces(response) {
							$scope.yourCity = response.data;
						}, function myError(response) {
							$scope.yourCity = response.statusText;
						});
					}
				});